%
% This is the LaTeX template file for lecture notes for CS267,
% Applications of Parallel Computing.  When preparing 
% LaTeX notes for this class, please use this template.
%
% To familiarize yourself with this template, the body contains
% some examples of its use.  Look them over.  Then you can
% run LaTeX on this file.  After you have LaTeXed this file then
% you can look over the result either by printing it out with
% dvips or using xdvi.
%

\documentclass[twoside]{article}
\setlength{\oddsidemargin}{0.25 in}
\setlength{\evensidemargin}{-0.25 in}
\setlength{\topmargin}{-0.6 in}
\setlength{\textwidth}{6.5 in}
\setlength{\textheight}{8.5 in}
\setlength{\headsep}{0.75 in}
\setlength{\parindent}{0 in}
\setlength{\parskip}{0.1 in}

%
% ADD PACKAGES here:
%

\usepackage{amsmath,amsfonts,graphicx}

%
% The following commands set up the lecnum (lecture number)
% counter and make various numbering schemes work relative
% to the lecture number.
%
\newcounter{lecnum}
\renewcommand{\thepage}{\thelecnum-\arabic{page}}
\renewcommand{\thesection}{\thelecnum.\arabic{section}}
\renewcommand{\theequation}{\thelecnum.\arabic{equation}}
\renewcommand{\thefigure}{\thelecnum.\arabic{figure}}
\renewcommand{\thetable}{\thelecnum.\arabic{table}}

%
% The following macro is used to generate the header.
%
\newcommand{\lecture}[4]{
   \pagestyle{myheadings}
   \thispagestyle{plain}
   \newpage
   \setcounter{lecnum}{#1}
   \setcounter{page}{1}
   \noindent
   \begin{center}
   \framebox{
      \vbox{\vspace{2mm}
    \hbox to 6.28in { {\hfill \bf Fall 2015: Analysis of Boolean
        Functions \hfill} }
       \vspace{4mm}
       \hbox to 6.28in { {\Large \hfill Lecture #1: #2  \hfill} }
       \vspace{2mm}
       \hbox to 6.28in { {\it Lecturer: #3 \hfill Scribes: #4} }
      \vspace{2mm}}
   }
   \end{center}
   \markboth{Lecture #1: #2}{Lecture #1: #2}

   {\bf Note}: {\it LaTeX template courtesy of UC Berkeley EECS dept
     and IITD.}

   {\bf Disclaimer}: {\it Errors and Omissions Expected.}
   \vspace*{4mm}
}

% Use these for theorems, lemmas, proofs, etc.
\newtheorem{theorem}{Theorem}[lecnum]
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{claim}[theorem]{Claim}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{observation}[theorem]{Observation}
\newenvironment{proof}{{\bf Proof:}}{\hfill\rule{2mm}{2mm}}

% **** IF YOU WANT TO DEFINE ADDITIONAL MACROS FOR YOURSELF, PUT THEM
% HERE:

\newcommand\E{\mathbb{E}}
\newcommand\F{\mathbb{F}}

\begin{document}
\lecture{3}{September 4}{Amit Sinhababu}{Satyadev Nandakumar}

\section{Application 1: Blum-Luby-Rubinfeld Linearity Test}

\begin{definition}
\label{def:linear}
A function $f: \F^n_2 \to \F_2$ is called \emph{linear} if $\forall x,
y \in \F_2^n$, we have $f(x+y) = f(x)+f(y)$.
\end{definition}

\begin{observation}
\label{obs:linear_sum}
A function $f: \F^n_2 \to \F_2$ is linear if and only if $\exists a_1,
\dots, a_n \in \F_2$ such that $\forall$ $x_1, \dots, x_n \in \F_2$,
we have
$f(x_1, \dots, x_n) = a_1 x_1 + \dots + a_n x_n$.
\end{observation}

\begin{proof}
Assume that $f$ is linear according to definition
\ref{def:linear}. Then
\begin{align*}
f(x_1, \dots, x_n) &= f(x_1 (1,0,\dots,0) + x_2 (0,1,\dots,0) +
x_n(0,0,\dots,1))\\
&= x_1 f(e_1) + \dots + x_n f(e_n).
\end{align*}

Conversely, if $f(x_1, \dots, x_n)$ is $\sum_{i=1}^n a_i x_i$, and
$f(y_1, \dots, y_n)$ is $\sum_{i=1}^n a_i y_i$, then $f(x_1+y_1,
\dots, x_n+y_n)$ is $\sum_{i=1}^n a_i x_i + a_i y_i$, which is
$f(x)+f(y)$. 
\end{proof}

\begin{observation}
A function is linear if and only if it is a parity function.
\end{observation}
\begin{proof}
Let $f$ be a linear function, and let 
$$S_f = \{i ~\mid~ f(e_i)>0\}.$$
By observation \ref{obs:linear_sum} $f(x_1, \dots, x_i)$ is the parity
of the bits $x_i$, $i \in S_f$. 

Conversely, let $f$ be the parity of bits $x_i$, $i \in S_f$. Then we
can write it in the form in \ref{obs:linear_sum}.
\end{proof}

Now, we come to the problem of linearity testing. We are given a
function $f: \F_2^n \to \F_2$, as a ``black box'' --- that is to say,
the only way to know $f$ is to give inputs to $f$ and observe the
outputs. \footnote{This contrasts with assuming that we know something
  about the actual algorithm or formula of $f$, which would correspond
  to ``gray box'' or ``white box'' testing.} Can we determine whether
$f$ is linear by querying it at various inputs? Questions of this
kind, where we determine whether a given function belongs to a larger
class of functions obeying some property, are called \emph{property
  testing}. 

Of course, if we test all $2^n$ possible inputs in $F_2^n$ then we
would know $f$ completely. It is also possible to show that
deterministically, these many queries are necessary to know $f$ fully
--- consider a strategy that makes fewer than $2^n$ queries. We can
construct two functions which match on all of those queries but differ
on one of the unqueried inputs.

So now, we ask if it is possible to ask very few questions ---
polynomially many in $n$ --- and still determine the linearity of $f$
with high probability?

To make the problem more precise, we have to say that if the input is
``very close''to being linear, or ``very far'' from being linear, then
the output classification will be correct with high probability. (A
function could neither be very close nor very far from linear
functions, in which case we indulge our algorithm to err.)  Here is
one way to define the closeness of two functions.

\begin{definition}
The \emph{distance} between two functions $f,g: \F_2 \to \F_2$ is
$$d(f,g) = Pr[x \in F_2 ~:~ f(x) \ne g(x)]$$
Functions $f, g : \F_2 \to \F_2$ are $\varepsilon$-close if
$d(f,g) \le \varepsilon$.
\end{definition}
($d$ is a metric on the space of functions from $\F_2^n$ to $\F_2$.)

\begin{lemma}
$$\langle f, g \rangle = 1 - 2d(f,g).$$
\end{lemma}

We want an algorithm that will accept the input function $f$ if it is
$\varepsilon$-close to some linear function, and will reject it if it
is more than $2\varepsilon$-far from every linear function. 

\section{Algorithm and Analysis}

{\bf Algorithm} Randomly choose two $x, y \in \F_2$. Accept $f$ if
$f(x+y) = f(x)+f(y)$. Reject $f$ otherwise.

Suppose $f$ is $\varepsilon$ close to some linear function. Then
$1-\varepsilon$ is the probability that $f(x+y)$ is different from
$f(x)+f(y)$. 

Now,
\[
Pr[(x,y) \mid f(x)+f(y) \ne f(x+y)] = 
Pr[(x,y) \mid f(x)f(y) \ne  f(x+y)]\\ 
\]
Using the indicator random variable $I(x,y)$ which is 1 if $f(x)f(y) =
f(x+y)$ and 0 otherwise, the above probability can be written as the
expectation $E_{(x,y)}[I(x,y)]$.

We can now rewrite the above expectation in the following special
form:
$$ E_{(x,y)}[I(x,y)] = E[1/2 + 1/2 f(x)f(y)f(x+y)].$$
Note that if $f(x)f(y)\ne f(x+y)$, then the inner expression becomes
0, and otherwise, the inner expression is 1. 

Linearity of expectation allows us to write the expected value as 
\begin{equation}
\label{eqn:linearity_test_bound}
1/2 + 1/2 E[f(x)f(y)f(x+y)].
\end{equation}

We now calculate $E[f(x)f(y)f(x+y)]$. We have 
\begin{align*}
E[f(x)f(y)f(x+y)] &= E_x [f(x) E_y[f(y) f(x+y)]]\\
                  &= E_x [f(x) E_y[f(y) f(x-y)]],
\end{align*}
since in $\F_2$, $f(x+y)$ is $f(x-y)$. Note that $E_y[f(y) f(x-y)]$ is
the convolution of $f$ with itself, denoted $f * f$. Thus the above
expression is
\begin{align*}
E_x[f(x)~f*f(x)] &= \langle f, f*f \rangle\\
                 &= \sum_{S \subseteq [n]} \hat{f}(S) \hat{f*f}(S)\\ 
                 &= \sum_{S \subseteq [n]} \hat{f}(S) \hat{f}(S)
                                           \hat{f}(S),
\end{align*}
the second step following by the Fourier representation of the
functions, and the third by the convolution formula.

Plugging this back into the equation \ref{eqn:linearity_test_bound},
we get 
$$\sum_{S \subseteq [n]} \hat{f}(S)^3 = 1 - 2\varepsilon.$$

Now, we could apply Parseval's identity if the left hand side were a
sum of squares. We can however, overestimate $\hat{f}(S)$ by the
maximum $\hat{f}(S)$ over all $S \subseteq [n]$. This yields
$$\max_{S \subseteq [n]} \hat{f}(S)~ \sum_{S \subseteq [n]}
\hat{f}(S)^2 =  \max_{S \subseteq[n]} \hat{f}(S) \ge 1 -
2\varepsilon.$$

Now, note that the maximum value of $\hat{f}(S)$ will be attained with
respect to the linear function $f$ is closest to --- for the Fourier
coefficient $\hat{f}(S)$ is just the length of the projection of $f$
along the linear function $\chi(S)$. We have
$$ \hat{f}(S) = 1- 2d(f,\chi(\text{argmax}(\hat{f}(S)))) \ge
1-2\varepsilon,$$ 
from which we get that
$$d(f, \chi(\text{argmax}(\hat{f}(S)))) \le \varepsilon.$$

\section{Local Correctibility}

\end{document}




