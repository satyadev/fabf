%
% This is the LaTeX template file for lecture notes for CS267,
% Applications of Parallel Computing.  When preparing 
% LaTeX notes for this class, please use this template.
%
% To familiarize yourself with this template, the body contains
% some examples of its use.  Look them over.  Then you can
% run LaTeX on this file.  After you have LaTeXed this file then
% you can look over the result either by printing it out with
% dvips or using xdvi.
%

\documentclass[twoside]{article}
\setlength{\oddsidemargin}{0.25 in}
\setlength{\evensidemargin}{-0.25 in}
\setlength{\topmargin}{-0.6 in}
\setlength{\textwidth}{6.5 in}
\setlength{\textheight}{8.5 in}
\setlength{\headsep}{0.75 in}
\setlength{\parindent}{0 in}
\setlength{\parskip}{0.1 in}

%
% ADD PACKAGES here:
%

\usepackage{amsmath,amsfonts,graphicx}

%
% The following commands set up the lecnum (lecture number)
% counter and make various numbering schemes work relative
% to the lecture number.
%
\newcounter{lecnum}
\renewcommand{\thepage}{\thelecnum-\arabic{page}}
\renewcommand{\thesection}{\thelecnum.\arabic{section}}
\renewcommand{\theequation}{\thelecnum.\arabic{equation}}
\renewcommand{\thefigure}{\thelecnum.\arabic{figure}}
\renewcommand{\thetable}{\thelecnum.\arabic{table}}

%
% The following macro is used to generate the header.
%
\newcommand{\lecture}[4]{
   \pagestyle{myheadings}
   \thispagestyle{plain}
   \newpage
   \setcounter{lecnum}{#1}
   \setcounter{page}{1}
   \noindent
   \begin{center}
   \framebox{
      \vbox{\vspace{2mm}
    \hbox to 6.28in { {\hfill \bf Fall 2015: Analysis of Boolean
        Functions \hfill} }
       \vspace{4mm}
       \hbox to 6.28in { {\Large \hfill Lecture #1: #2  \hfill} }
       \vspace{2mm}
       \hbox to 6.28in { {\it Lecturer: #3 \hfill Scribes: #4} }
      \vspace{2mm}}
   }
   \end{center}
   \markboth{Lecture #1: #2}{Lecture #1: #2}

   {\bf Note}: {\it LaTeX template courtesy of UC Berkeley EECS dept
     and IITD.}

   {\bf Disclaimer}: {\it Errors and Omissions Expected.}
   \vspace*{4mm}
}

% Use these for theorems, lemmas, proofs, etc.
\newtheorem{theorem}{Theorem}[lecnum]
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{claim}[theorem]{Claim}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{observation}[theorem]{Observation}
\newenvironment{proof}{{\bf Proof:}}{\hfill\rule{2mm}{2mm}}

% **** IF YOU WANT TO DEFINE ADDITIONAL MACROS FOR YOURSELF, PUT THEM
% HERE:

\newcommand\E{\mathbb{E}}
\newcommand\F{\mathbb{F}}

\begin{document}
\lecture{3}{September 11}{Amit Sinhababu}{Satyadev Nandakumar}

\section{Voting functions and influence}
We will view boolean functions $f: \{-1,1\}^n \to \{-1,1\}$ as voting
rules. For example, the following functions have intuitive interpretations
as voting rules.

\begin{enumerate}
\item $f(x_1, \dots, x_n) = x_1$. This is called the dictator rule, since
the outcome is decided by exactly one co-ordinate.
\item $f(x_1, x_2, x_3) = x_1 x_2$ is the majority function on $\{-1,1\}^3$.
\item Similarly, $f(x_1, x_2, x_3, x_4) = x_1x_2x_3x_4$ is the parity.
\item AND, OR and similar boolean functions.
\item Weighted majority functions are voting rules, sometimes called
linear threshold functions.
\end{enumerate}

A special class of Boolean functions which will be of interest to us is
called a $k$-junta. 

\begin{definition}
A function $f: \{-1,1\}^n \to \{-1,1\}$ is called a $k$-junta if it is a 
function that depends only on $k$ variables, where $k \le n$.
\end{definition}

The following quantity will be useful in our study of Boolean functions
using Fourier analysis. First, we will give a combinatorial definition
for the concept. Later, we will provide an equivalent Fourier-analytic
definition for the same.

\begin{definition}
We say that an input $x_i$ is \emph{pivotal} for function
$f:\{-1,1\}\to\{-1,1\}$ if
$$f(x_1, x_2, \dots, x_{i-1},{\bf x_{i}},x_{i+1}, \dots, x_n)
 \ne
f(x_1, x_2, \dots, x_{i-1},{\bf -x_{i}},x_{i+1}, \dots, x_n),$$
denoted as $f(x) \ne f(x^{\oplus i})$. 
\end{definition}

\begin{definition}
The \emph{influence} of a variable $x_i$ on function $f$, denoted
$\text{Influence}_i(f)$ is defined as 
$$P \{ (x_1, \dots, x_n) \in \{-1,1\}^n \mid
  f(x_1, \dots, x_{i-1},x_{i},x_{i+1}, \dots, x_n) \ne
  f(x_1, \dots, x_{i-1},-x_{i},x_{i+1}, \dots, x_n)\}.$$
The \emph{total influence} on a function $f:\{-1,1\}^n \to \{-1,1\}$ 
is the sum of influences of all the $n$ variables.
\end{definition}

Thus the influence of a variable $x_i$ is the probability that $x_i$
is pivotal for a random input.

Before giving the Fourier analytic definition of influence, let us
compute the total influence of a few functions. We will show that
influence, as the term suggests, is a measure of how many variables
the function actually depends on.

\subsection{Influence of the dictator function}
In the dictator function, only one co-ordinate (without less of
generality, $x_1$). Influence$_1(dictator)$ is the probability that in
a randomly drawn vector $(x_1, \dots, x_n)$, flipping $x_1$ will cause
the output to change. This is clearly $1$.

\subsection{Influence of the parity function}
In the parity function, for any input $(x_1, \dots, x_n) \in
\{-1,1\}^n$ and any $i \in [1,n]$, flipping $x_i$ will cause the
output to change. Hence the influence of each bit is 1, implying that
the total influence of the function is $n$.

\subsection{Influence of the majority function}
We now analyze the influence that each bit has on the majority
function. First, let us assume that $n$ is odd. Then the variable
$x_1$ is pivotal only when exactly half of $x_2, \dots, x_n$ are 1 and
the other half are -1. This happens with probability
$$\frac{{n \choose n/2} }{2^n}.$$
We can obtain an asymptotic approximation using Stirling's 
approximation,
$$ n! \approx \sqrt{2n \pi} \left(\frac{n}{e}\right)^n,$$
whence we get that the probability is approximately
$$\frac{1}{\sqrt{2n\pi}}.$$

Thus the total influence of the majority function is 
$\Theta(1/\sqrt{n}) n = \Theta(\sqrt{n})$.

\subsection{Influence of the AND function}
$x_1$ will be pivotal for AND only if $x_2, \dots, x_{n}$ are all
1. This happens with probability $\frac{1}{2^n}$. Hence the total
influence of the function is $n/2^n$

\subsection{Influence of the $k$-junta.}
As in the dictator function, if a variable is not a member of the
$k$-junta, then it is not influential. The influence of a member of
the junta is 1. Hence the total influence of the function is $k$.

\subsection{The TRIBES counterexample}
It is natural to ask whether $I(f) \le k$ implies that the function is
a $k$-junta. This need not be true. The following function, despite
not being a $k$-junta, has total influence $\le k$.

A tribe is a formula of the following kind:
$$(x_1 \wedge \dots \wedge x_n) \vee
  (x_{n+1} \wedge \dots \wedge x_{2n}) \vee
  \dots
  (x_{(p-1)n+1} \wedge \dots \wedge x_{pn}).$$

We now compute the influence, without loss of generality, of
$x_1$. $x_1$ is pivotal if $x_2, \dots, x_n$ are all 1, and
at least one variable among each of the blocks $x_{jn+1}, \dots,
x_{(j+1)n}$, $1 \le j < n$ is 0. This happens with probability 
$$\frac{1}{2^{n-1}} \left(1 - \frac{1}{2^n}\right)^{p-1}.$$
Thus every variable has strictly positive influence. Thus TRIBES is
not a $k$-junta, for $k<n$.

The total influence of the function is
$$np\frac{1}{2^{n-1}} \left(1 - \frac{1}{2^n}\right)^{p-1}.$$ 

Fix $p$. Clearly, the value is $o\left(\frac{1}{2^n}\right)$. Hence
for a fixed $k$, for all sufficiently large $n$, the total influence
of TRIBES will be less than $k$.


\end{document}
